@extends('layout')
@section('content')
<style>
  .espacio{
    margin-top: 40px;
  }
</style>
<div class="espacio">
    <h1>Inventorio de Empleados</h1>
    <hr/>
    <button class="btn btn-primary" onclick="window.location='{{ route("empleados.create") }}'">Agregar un nuevo empleado</button>
    @if(session()->get('mensaje'))
        <div class="alert alert-{{ strpos(session()->get('mensaje') , 'eliminado') ? 'danger' : 'success'}}">
            {{session()->get('mensaje') }}
        </div><br/>
    @endif
    <table class="table table-striped">
        <thead>
            <tr>
                <td>ID</td>
                <td>Nombre</td>
                <td>Edad</td>
                <td>Salario</td>
                <td>Editar</td>
                <td>Borrar</td>
            </tr>
        </thead>
        <tbody>
            @foreach($empleados as $empleado)
            <tr>
                <td>{{ $empleado->id}}</td>
                <td>{{ $empleado->nombre}}</td>
                <td>{{ $empleado->edad}}</td>
                <td>{{ $empleado->salario}}</td>
                <td><a href="{{ route('empleados.edit', $empleado->id)}}" class="btn btn-primary">Editar</a></td>
                <td>
                    <form action="{{ route('empleados.destroy', $empleado->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button onclick = "return confirm('Estas seguro de borrar este empleado?')" class="btn btn-danger" type="submit">Borrar</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $empleados->links()}}
</div>
@endsection