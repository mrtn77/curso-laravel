@extends('layout')
@section('content')
<style>
  .espacio {
    margin-top: 40px;
  }
  .card-header{
      background-color: #3490dc;
      color: #ffffff;
  }
</style>

<div class="card espacio">
    <div class="card-header">
        Agregar Empleado
    </div>
    <div class="card-body">
        @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br/>
        @endif
        <form action="{{ route('empleados.store')}}" method="post">
            @csrf
            <div class="form-group">
              <label for="nombre">Nombre:</label>
              <input type="text" class ="form-control" name="nombre" value="{{old('nombre')}}">
            </div>
            <div class="form-group">
              <label for="edad">Edad:</label>
              <input type="text" class ="form-control" name="edad" value="{{old('edad')}}">
            </div>
            <div class="form-group">
              <label for="salario">Salario:</label>
              <input type="text" class ="form-control" name="salario" value="{{old('salario')}}">
            </div>
            <button type="button" class="btn btn-success" onclick="window.location='{{ url("empleados")}}'">Regresar</button>
            <button type="submit" class="btn btn-primary">Agregar</button>
        </form>
    </div>
</div>
@endsection
