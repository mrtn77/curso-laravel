@extends('layout')
@section('content')
<style>
  .espacio {
    margin-top: 40px;
  }
  .card-header{
      background-color: #3490dc;
      color: #ffffff;
  }
</style>

<div class="card espacio">
    <div class="card-header">
        Editar Empleado
    </div>
    <div class="card-body">
        @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br/>
        @endif
        <form action="{{ route('empleados.update', $empleado->id) }}" method="post">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="nombre">Nombre: </label>
                <input type="text" class="form-control" name="nombre" value="{{ $empleado->nombre }}">
            </div> 
            <div class="form-group">
                <label for="edad">Edad: </label>
                <input type="text" class="form-control" name="edad" value="{{ $empleado->edad }}">
            </div> 
            <div class="form-group">
                <label for="salario">Salario: </label>
                <input type="text" class="form-control" name="salario" value="{{ $empleado->salario }}">
            </div>
            <button type="submit" class="btn btn-primary">Actualizar</button>           
        </form>
    </div>
</div>
@endsection